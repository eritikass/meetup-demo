FROM node:10-alpine AS source

WORKDIR /app
COPY ./ /app

RUN npm install --production

FROM node:10-alpine

ENV PORT=3000

WORKDIR /app
COPY --from=source /app /app

EXPOSE 3000

CMD ["node", "src/main.js"]
