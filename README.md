# meetup-demo

 * slides: [https://docs.google.com/presentation/d/1Bcg72ZgmTaaMxV8qT7ixru5tW-fWE8-7uSSTMkw28-4](https://docs.google.com/presentation/d/1Bcg72ZgmTaaMxV8qT7ixru5tW-fWE8-7uSSTMkw28-4)
 * [traefik](https://traefik.io/) setup: [https://github.com/eritikass/traefik-setup](https://github.com/eritikass/traefik-setup)
 
> master deployment - [http://master.awstest1.kassikas.com/](http://master.awstest1.kassikas.com/)

## Project setup
```
npm install
```

### Run project
```
npm run start
```

### Run your tests
```
yarn run test
```

### Run linter
```
npm run lint
```

### Run your unit tests
```
npm run test
```
