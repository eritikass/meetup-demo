const express = require('express');
const sum = require('./sum');

const app = express();
const port = process.env.PORT || 3000;

app.get('/', (req, res) => res.send(`
Hello pipedrive!


`));

app.get('/sum/:num1/:num2', (req, res) => res.send(`
  ${req.params.num1}+${req.params.num2}=${sum(req.params.num1, req.params.num2)}
`));

// eslint-disable-next-line no-console
app.listen(port, '0.0.0.0', () => console.log(`
  Example app listening on port http://localhost:${port}/!

sum-test:   http://localhost:${port}/sum/2/3
  
 `));
