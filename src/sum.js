const sum = (a, b) => {
  const s = Number(a) + Number(b);
  return s;
};

module.exports = sum;
